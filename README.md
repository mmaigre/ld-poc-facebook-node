# LionDesk POC: Node Facebook SDK

## ***Overview***:
Proof of concept that integrates a third-party Nodejs Facebook SDK for the application of custom audiences in LionDesk.

## ***Dependencies***:
* Express
* nodemon/rimraf: File watching/recompiling
* **fb**: Node Facebook SDK
* babel / babel-preset-es2015: Used babel for transpiling ES6+ Code


## ***How to use***:

Access token (provided by FB API) must be sent as a Bearer Token in the headers (Authorization Header) for all requests.

_NPM Commands_:
* `npm run serve` (Runs npm 'dev' and 'build' with nodemon for file changes) Use this to run local dev server




### GET /ad-accounts ###
**Authorization**: Bearer ${token}

**Description**: /me/adaccounts/ from FB API (Gets all ad-accounts)

**Response**: 
```
[
	{
		"account_id": "123456",
		"id": "act_123456"
	},
	{
		"account_id": "12345678",
		"id": "act_12345678"
	}
]
```

***
***

### GET /custom-audiences ###
**Authorization**: Bearer ${token}

**Query Parameters**:
> ad_account_id

**Description**: /custom-audiences from FB API (Gets all custom audiences)

**Response**: 
```
[
	{
		"id": "123456789"
	},
	{
		"id": "123456781"
	}
]
```

***
***

### POST /custom-audiences ###
**Authorization**: Bearer ${token}

**Query Parameters**:  

> ad_account_id: Ad Account ID

**Body**: 
> name: Custom audience name

**Description**: /custom-audiences from FB API (Creates a new custom audience)

**Response**: 
```
[
	{
		"id": "123456789"
	},
]
```

***
***

### POST /custom-audiences/:id/users ###

**Authorization**: Bearer ${token}

**URL Parameters**:  
> id: Custom Audience ID


**Body**: 
```
{
    "users": [
        {
            "FN": "First Name",
            "LN": "Last Name",
            "EMAIL": "email@example.com"
        }
    ]
}
```

**Description**: Adds users to the Custom Audience

**Response**: 
```
{
	"audience_id": "123123123123",
	"session_id": "6102111515777012498",
	"num_received": 1,
	"num_invalid_entries": 0,
	"invalid_entry_samples": {}
}
```


### DELETE /custom-audiences/:id/users ###
**Authorization**: Bearer ${token}

**URL Parameters**:  
> id: Custom Audience ID


**Body**: 
```
{
    "users": [
        {
            "FN": "First Name",
            "LN": "Last Name",
            "EMAIL": "email@example.com"
        }
    ]
}
```

**Description**: Deletes users from the Custom Audience

**Response**: 
```
{
	"audience_id": "123123123123",
	"session_id": "6102111515777012498",
	"num_received": 1,
	"num_invalid_entries": 0,
	"invalid_entry_samples": {}
}
```