import crypto from 'crypto';

const hashSHA256 = (value) => {
    const hash = crypto.createHash('sha256');
    hash.update(value);
    const hex = hash.digest('hex');
    return hex;
};

export default hashSHA256;