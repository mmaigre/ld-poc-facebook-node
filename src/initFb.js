import FB, { FacebookApiException } from 'fb';

const initFb = () => {
    FB.options({version: 'v6.0'});
}

export const setAccessToken = (token) =>{
    FB.setAccessToken(token);
}

export default initFb;