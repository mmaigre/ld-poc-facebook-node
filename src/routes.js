import express from 'express';
import { createCustomAudience, getCustomAudiences, addRemoveUsers } from './services/custom-audiences';
import { getAdAccounts } from './services/ad-accounts';

const router = express.Router();

router.get('/ad-accounts', getAdAccounts);
router.get('/custom-audiences', getCustomAudiences);
router.post('/custom-audiences', createCustomAudience);
router.post('/custom-audiences/:id/users', addRemoveUsers);
router.delete('/custom-audiences/:id/users', addRemoveUsers);

export default router;