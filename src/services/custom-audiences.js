import FB from 'fb';

import hash from './../utils/hashSHA256';

const getCustomAudiences = (req, res) => {
    const { ad_account_id } = req.query;

    if (!ad_account_id) return res.json({ paramsError: 'ad_account_id is a required query param' });

    FB.api(`/act_${ad_account_id}/customaudiences`, fbRes => {
        if(!fbRes || !fbRes.data || fbRes.error) {
            console.log(!fbRes ? 'ERROR getting custom audiences' : fbRes.error);
            res.json(fbRes)
            return;
        }
        res.json(fbRes.data);
    });
}

const createCustomAudience = (req, res) => {
    const { ad_account_id } = req.query;
    const { name } = req.body;
    if (!name) return res.json({ paramsError: 'name is a required body param' });
    if (!ad_account_id) return res.json ({ queryError: 'ad_account_id is a required query param'});

    const body = {
        customer_file_source: 'USER_PROVIDED_ONLY',
        subtype: 'CUSTOM',
        name,
    }

    FB.api(`/act_${ad_account_id}/customaudiences`, 'post', body, fbRes => {
        if(!fbRes || fbRes.error) {
            console.log(!fbRes ? 'ERROR creating custom audience' : fbRes.error);
            res.json(fbRes)
            return;
        }
        res.json(fbRes)
    });
}

const addRemoveUsers = (req, res) =>{
    const { id } = req.params;
    const { users } = req.body;
    const method = req.method.toLowerCase();
    const schema = ["FN", "LN", "EMAIL"];
    const payload = {
        schema,
        data: users.map(user=>{
            return Object.values(user).map( value => hash(value) );
        })
    }

    if (!id) return res.json({ paramsError: 'id is a required URL param' });

    FB.api(`/${id}/users`, method, { payload }, fbRes => {
        if(!fbRes || fbRes.error) {
            console.log(!fbRes ? 'ERROR updating custom audience\'s users' : fbRes.error);
            res.json(fbRes)
            return;
        }
        res.json(fbRes)
    });
}


export {
    createCustomAudience,
    getCustomAudiences,
    addRemoveUsers
};