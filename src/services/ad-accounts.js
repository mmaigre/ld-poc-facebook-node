import FB from 'fb';

const getAdAccounts = (req, res) => {
    FB.api(`/me/adaccounts/`, fbRes => {
        if(!fbRes || !fbRes.data || fbRes.error) {
            console.log(!fbRes ? 'ERROR getting Ad accounts' : fbRes.error);
            res.json(fbRes);
            return;
        }
        res.json(fbRes.data);
    });
}

export {
    getAdAccounts
};