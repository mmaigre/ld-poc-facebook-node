import express from 'express';
import initFb, { setAccessToken } from './initFb';
import routes from './routes';

const app = express();
const { PORT } = process.env;

app.use(express.json())

app.use((req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(403).json({ authError: 'FB Access Token required. Pass it as a bearer token' });
  }
  setAccessToken(req.headers.authorization.replace("Bearer ", ""))
  next();
});


app.use(routes);
app.listen(PORT, () => {
  initFb();
  console.log(`Server listening on port: ${PORT}`);
});